# MusicalGamecontrollers

This is an attempt to use a Wiimote (with MotionPlus) as good as possible to make sounds/music with it. I'm planning to do this by either generating midi signals or by creating a synthesizer directly.

## Notes on the Wiimote

- it sends data at a frequency of ~100Hz
