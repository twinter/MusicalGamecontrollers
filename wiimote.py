"""
Extension for the cwiid Wiimote lib.

Requires a Wiimote with MotionPlus.

Currently no support planned for:
- callbacks for everything except the Wiimote itself and the nunchuck
"""
import math
import time
import logging
from typing import List, Tuple, Any, Callable

import cwiid

import settings


class Wiimote(cwiid.Wiimote):
	def __init__(self, *args, logger: logging.Logger, **kwargs):
		self.logger = logger
		
		# buffers for the unreadable properties of the parent
		self._led = 0
		self._mesg_callback = None
		self._rpt_mode = 0
		self._rumble = False
		self._enabled = 0
		
		super().__init__(*args, **kwargs)

		# load accelerometer calibration values
		zero, gravity = self.get_acc_cal(cwiid.EXT_NONE)
		# neutral position for the linear accelerometers
		self._acc_lin_zero = list(zero)
		# scale factor for the lin. accelerometer (how many units for 1g?)
		self._acc_lin_scale = list([gravity[i] - zero[i] for i in range(3)])
		
		# object variables
		self.acc_lin = [.0, .0, .0]
		self._acc_lin_offset = [.0, .0, 1.]  # offset due to gravity
		self.acc_ang = [.0, .0, .0]
		self._acc_ang_zero = [8192, 8192, 8192]
		self.velocity = [.0, .0, .0]
		self.position = [.0, .0, .0]
		self.orientation = [.0, .0, .0]  # pitch, yaw, roll
		self.last_cycle = 0  # timestamp of the start of the last cycle
		self._calibration_active = False  # this os controlled by the calibration_active property
		self._calibration_end = None
		self._calibration_lin_min = None
		self._calibration_lin_max = None
		self._calibration_ang_min = None
		self._calibration_ang_max = None
		self._progress_active = False
		self._progress_last_led = None  # last state of the LEDs when progress is shown
		self._last_btn_state = 0
		
		# callbacks, the first argument will always be a timestamp
		self.on_new_message = None
		self.on_disconnect = None
		self.on_btn_1 = None
		self.on_btn_2 = None
		self.on_btn_a = None
		self.on_btn_b = None
		self.on_btn_up = None
		self.on_btn_down = None
		self.on_btn_left = None
		self.on_btn_right = None
		self.on_btn_home = None
		self.on_btn_plus = None
		self.on_btn_minus = None
		self.on_btn_1_release = None
		self.on_btn_2_release = None
		self.on_btn_a_release = None
		self.on_btn_b_release = None
		self.on_btn_up_release = None
		self.on_btn_down_release = None
		self.on_btn_left_release = None
		self.on_btn_right_release = None
		self.on_btn_home_release = None
		self.on_btn_plus_release = None
		self.on_btn_minus_release = None
		
		self.logger.info('ready, battery at {:.1f}%'.format(self.battery_level * 100))
	
	@property
	def led(self) -> int:
		""" Status of the bits equals the status of the LEDs """
		return self._led
	
	@led.setter
	def led(self, v: int):
		if not self._progress_active:
			cwiid.Wiimote.led.__set__(self, v)
		self._led = v
	
	@property
	def mesg_callback(self) -> callable:
		""" will be called when a new message is available. """
		# TODO: investigate why this doesn't work
		return self._mesg_callback
	
	@mesg_callback.setter
	def mesg_callback(self, f: callable):
		cwiid.Wiimote.mesg_callback.__set__(self, f)
		self._mesg_callback = f
	
	@property
	def rpt_mode(self) -> int:
		""" Defines what will be reported by the Wiimote. """
		return self._rpt_mode
	
	@rpt_mode.setter
	def rpt_mode(self, v: int):
		cwiid.Wiimote.rpt_mode.__set__(self, v)
		self._rpt_mode = v
	
	@property
	def rumble(self) -> int:
		""" Turns rumble on or off. """
		return self._rumble
	
	@rumble.setter
	def rumble(self, v: bool):
		cwiid.Wiimote.rumble.__set__(self, v)
		self._rumble = v
	
	@property
	def enabled(self) -> int:
		""" Defines what features are enabled on the Wiimote. """
		return self._enabled
	
	@enabled.setter
	def enabled(self, v: int):
		super().enable(v)
		self._enabled = v
	
	@property
	def battery_level(self) -> float:
		""" The battery level in range [0-1]. """
		return int(self.state['battery']) / cwiid.BATTERY_MAX
	
	@property
	def calibration_active(self):
		return self._calibration_active
	
	@calibration_active.setter
	def calibration_active(self, value: bool):
		if value:
			if not self._calibration_active:
				self.logger.info('starting calibration, do not move the Wiimote')
			self._calibration_end = time.time() + settings.calibration_duration
			self._calibration_lin_min = None
			self._calibration_lin_max = None
			self._calibration_ang_min = None
			self._calibration_ang_max = None
		elif self._calibration_active:
			self.progress_disable()
		self._calibration_active = value
	
	def timed_rumble(self, duration=0.15):
		"""
		Activates vibration for a certain time.
		Careful: this functions is blocking!
		
		:param duration: duration of the vibration in seconds
		"""
		
		self.rumble = True
		time.sleep(duration)
		self.rumble = False
	
	def progress_display(self, percent: float) -> None:
		"""
		Displays progress information with the LEDs.
		
		:param percent: value from [0, 100]
		"""
		if percent < 20:
			led_next = 0b0
		else:
			led_next = cwiid.LED1_ON
		
		if percent >= 40:
			led_next |= cwiid.LED2_ON
		if percent >= 60:
			led_next |= cwiid.LED3_ON
		if percent >= 80:
			led_next |= cwiid.LED4_ON
			
		if led_next != self._progress_last_led:
			cwiid.Wiimote.led.__set__(self, led_next)
			self._progress_last_led = led_next
	
	def progress_disable(self) -> None:
		self._progress_active = False
		self._progress_last_led = None
		self.led = self.led  # reset to original LED state
	
	# noinspection PyMethodMayBeStatic
	def _call_callback(self, f: Callable, timestamp: float = None, *args, **kwargs) -> None:
		if timestamp is None:
			timestamp = time.time()
			
		if callable(f):
			f(timestamp, *args, **kwargs)
	
	def _btn_callback(
			self,
			name: str,
			btn_state: int,
			value: int,
			f: Callable,
			timestamp: float
	):
		if btn_state & value != 0:
			self.logger.debug('button {}'.format(name))
			self._call_callback(f, timestamp)
	
	def _handle_buttons(self, btn_state: int, t: float):
		# shorten callback helper to reduce line length
		cb = self._btn_callback
		
		# filter and handle button presses
		btn_presses = btn_state & ~self._last_btn_state
		cb('1 pressed', btn_presses, cwiid.BTN_1, self.on_btn_1, t)
		cb('2 pressed', btn_presses, cwiid.BTN_2, self.on_btn_2, t)
		cb('A pressed', btn_presses, cwiid.BTN_A, self.on_btn_a, t)
		cb('B pressed', btn_presses, cwiid.BTN_B, self.on_btn_b, t)
		cb('up pressed', btn_presses, cwiid.BTN_UP, self.on_btn_up, t)
		cb('down pressed', btn_presses, cwiid.BTN_DOWN, self.on_btn_down, t)
		cb('left pressed', btn_presses, cwiid.BTN_LEFT, self.on_btn_left, t)
		cb('right pressed', btn_presses, cwiid.BTN_RIGHT, self.on_btn_right, t)
		cb('home pressed', btn_presses, cwiid.BTN_HOME, self.on_btn_home, t)
		cb('plus pressed', btn_presses, cwiid.BTN_PLUS, self.on_btn_plus, t)
		cb('minus pressed', btn_presses, cwiid.BTN_MINUS, self.on_btn_minus, t)
		
		# filter and handle button releases
		btn_releases = ~btn_state & self._last_btn_state
		cb('1 released', btn_releases, cwiid.BTN_1, self.on_btn_1_release, t)
		cb('2 released', btn_releases, cwiid.BTN_2, self.on_btn_2_release, t)
		cb('A released', btn_releases, cwiid.BTN_A, self.on_btn_a_release, t)
		cb('B released', btn_releases, cwiid.BTN_B, self.on_btn_b_release, t)
		cb('up released', btn_releases, cwiid.BTN_UP, self.on_btn_up_release, t)
		cb('down released', btn_releases, cwiid.BTN_DOWN, self.on_btn_down_release, t)
		cb('left released', btn_releases, cwiid.BTN_LEFT, self.on_btn_left_release, t)
		cb('right released', btn_releases, cwiid.BTN_RIGHT, self.on_btn_right_release, t)
		cb('home released', btn_releases, cwiid.BTN_HOME, self.on_btn_home_release, t)
		cb('plus released', btn_releases, cwiid.BTN_PLUS, self.on_btn_plus_release, t)
		cb('minus released', btn_releases, cwiid.BTN_MINUS, self.on_btn_minus_release, t)

		self._last_btn_state = btn_state
		
	def _handle_calibration(self, acc_lin: List[int], acc_ang: List[int]) -> None:
		if self._calibration_lin_min is None or self._calibration_lin_max is None:
			self._calibration_lin_min = acc_lin.copy()
			self._calibration_lin_max = acc_lin.copy()
		else:
			for i in range(3):
				if acc_lin[i] < self._calibration_lin_min[i]:
					self._calibration_lin_min[i] = acc_lin[i]
				elif acc_lin[i] > self._calibration_lin_max[i]:
					self._calibration_lin_max[i] = acc_lin[i]
		
		if self._calibration_ang_min is None or self._calibration_ang_max is None:
			self._calibration_ang_min = acc_ang.copy()
			self._calibration_ang_max = acc_ang.copy()
		else:
			for i in range(3):
				if acc_ang[i] < self._calibration_ang_min[i]:
					self._calibration_ang_min[i] = acc_ang[i]
				elif acc_ang[i] > self._calibration_ang_max[i]:
					self._calibration_ang_max[i] = acc_ang[i]
		
		# check if we're over the tolerance
		for i in range(3):
			delta_lin = self._calibration_lin_max[i] - self._calibration_lin_min[i]
			delta_ang = self._calibration_ang_max[i] - self._calibration_ang_min[i]
			out_of_limits = delta_lin > settings.calibration_tolerance_lin
			out_of_limits |= delta_ang > settings.calibration_tolerance_ang
			if out_of_limits:
				# only show the message after a bit of time passed to avoid spam
				if time.time() > self._calibration_end - settings.calibration_duration * .75:
					self.logger.warning('restarting calibration, too much movement')
				self.calibration_active = True  # resets the calibration
				break
		
		# calculate progress and check if we're done
		if self._calibration_end <= time.time():
			for i in range(3):
				delta_lin = (self._calibration_lin_max[i] - self._calibration_lin_min[i])
				delta_ang = (self._calibration_ang_max[i] - self._calibration_ang_min[i])
				delta_lin /= 2
				delta_ang /= 2
				delta_lin += self._calibration_lin_min[i]
				delta_ang += self._calibration_ang_min[i]
				self._acc_lin_zero[i] = delta_lin
				self._acc_ang_zero[i] = delta_ang
				
			self.calibration_active = False
			self.logger.info('calibration successful')
			self.timed_rumble()
		else:
			time_remaining = self._calibration_end - time.time()
			percent = 1 - time_remaining / settings.calibration_duration
			percent *= 100
			self.progress_display(percent)
	
	def _handle_orientation(
			self, acc_lin: List[int],
			acc_ang: List[int],
			acc_ang_low_speed: List[bool],
			timestamp: float
	) -> None:
		"""
		Converts the acceleration data in usable units and calculates position changes
		
		Assignment of the axes (acc_lin):
		___ is positive when the controller is subjected to an accelerating force directed ___
		x: to the right side
		y: forward
		z: downwards (i.e. gravity when laying normally)
		
		:param acc_lin: linear acceleration as received by the sensor
		:param acc_ang: angular acceleration as received by the sensor
		"""
		
		if self.calibration_active:
			self._handle_calibration(acc_lin, acc_ang)
			return
		
		# convert linear acceleration, subtract gravity
		for i in range(3):
			x = acc_lin[i]
			x -= self._acc_lin_zero[i]
			if -1 * settings.acc_lin_deadzone <= x <= settings.acc_lin_deadzone:
				x = 0
			x /= self._acc_lin_scale[i]
			x -= self._acc_lin_offset[i]
			self.acc_lin[i] = x
		
		# TODO: convert orientation and adapt self.orientation
		self.acc_ang = acc_ang
		
		cycle_time = timestamp - self.last_cycle
		
		# TODO: take orientation into account
		self.velocity = [self.velocity[i] + self.acc_lin[i] / cycle_time for i in range(3)]
		self.position = [self.position[i] + self.velocity[i] / cycle_time for i in range(3)]
	
	def _handle_messages(self, messages: List[Tuple[int, Any]], timestamp: float):
		# collects the logging info we build up when handling the message
		log_data = []
		
		# variables to save accelerometer data in
		acc_lin = None
		acc_ang = None
		
		for m in messages:
			if m[0] == cwiid.MESG_ERROR:
				if m[1] == cwiid.ERROR_DISCONNECT:
					self.logger.warning('Wiimote disconnected')
					self._call_callback(self.on_disconnect, timestamp=timestamp)
				elif m[1] == cwiid.ERROR_COMM:
					self.logger.warning('connection lost')
			elif m[0] == cwiid.MESG_STATUS:
				log_data.append('status: {}'.format(m[1]))
			elif m[0] == cwiid.MESG_ACC:
				log_data.append('acc: {}'.format(m[1]))
				acc_lin = m[1]
			elif m[0] == cwiid.MESG_IR:
				log_data.append('ir: {}'.format(m[1]))
			elif m[0] == cwiid.MESG_MOTIONPLUS:
				log_data.append('motionplus: {}'.format(m[1]))
				acc_ang = m[1]
			elif m[0] == cwiid.MESG_NUNCHUK:
				log_data.append('nunchuck: {}'.format(m[1]))
			elif m[0] == cwiid.MESG_BTN:
				self._handle_buttons(m[1], timestamp)
			# no support for the following
			elif m[0] == cwiid.MESG_BALANCE:
				log_data.append('balance: {}'.format(m[1]))
			elif m[0] == cwiid.MESG_CLASSIC:
				log_data.append('classic: {}'.format(m[1]))
			elif m[0] == cwiid.MESG_UNKNOWN:
				log_data.append('unknown: {}'.format(m[1]))
		
		if len(log_data) != 0:
			log_data.sort()
			self.logger.debug('data received: ' + str.join(' ', log_data))
			
		if acc_lin is not None and acc_ang is not None:
			acc_lin = list(acc_lin)
			acc_ang, acc_ang_low_speed = list(acc_ang['angle_rate']), list(acc_ang['low_speed'])
			for i in range(3):
				acc_lin[i] = int(acc_lin[i])
				acc_ang[i] = int(acc_ang[i])
				acc_ang_low_speed[i] = acc_ang_low_speed[i] != '0'
			self._handle_orientation(acc_lin, acc_ang, acc_ang_low_speed, timestamp)
	
	def update(self):
		# clear message queue, save only the last received pack of messages
		# i'd rather use the callback but it doesn't work
		messages_new = None
		while True:
			messages = messages_new
			messages_new = self.get_mesg()
			if messages_new is None:
				break
		
		if messages is not None:
			# get a timestamp for later use
			timestamp = time.time()
		
			self._handle_messages(messages, timestamp)
			self._call_callback(self.on_new_message, timestamp)
			
			self.last_cycle = timestamp
