import logging


log_level = logging.INFO

# target cycle frequency in the main loop in Hz
target_fps = 10000

# size of the deadzone (range of the all values: 256 points)
acc_lin_deadzone = 2

# length of a calibration phase in seconds
calibration_duration = 4

# amount of noise in points we allow during calibration
calibration_tolerance_lin = 5
calibration_tolerance_ang = 50
