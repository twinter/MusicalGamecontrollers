import math
import time
import logging

import audiolazy
import cwiid

import settings
from wiimote import Wiimote

from midihandler import MidiHandler


def remap(
		value: float,
		from_start: float, from_end: float,
		to_start: float, to_end: float,
		enforce_bounds: bool = False
) -> float:
	""" Map/convert/translate a value from one range of values to another. """
	
	# calculate the range on both sides
	from_range = from_end - from_start
	to_range = to_end - to_start
	
	# convert the value from the original scale to a neutral one (from 0 to 1)
	normalized = (value - from_start) / from_range if from_range != 0 else 1
	
	# convert the value from the neutral scale to the target one
	ret = to_start + normalized * to_range
	
	if enforce_bounds:
		if ret < min(to_start, to_end):
			ret = min(to_start, to_end)
		elif ret > max(to_start, to_end):
			ret = max(to_start, to_end)
	
	return ret


class Main:
	def __init__(self):
		self.wm: Wiimote = None
		self.stop = False
		self.enable_midi = False
		
		self.logger = logging.getLogger('Main')
		logging.basicConfig(
				format='%(asctime)s %(levelname)s %(name)s - %(message)s',
				level=settings.log_level
		)
		
		# self.midihandler = MidiHandler()
		self.midihandler = None
			
	def connect_wiimote(self):
		# create a logger for the Wiimote and connect to it
		logger_child: logging.Logger = self.logger.getChild('wiimote')
		logger_child.getEffectiveLevel()
		print('press the buttons 1 and 2')
		wm = Wiimote(logger=logger_child)

		# set the features we need
		wm.led = cwiid.LED1_ON
		# not using cwiid.FLAG_CONTINUOUS as we don't need updates when nothing changed
		wm.enabled = cwiid.FLAG_MESG_IFC | cwiid.FLAG_REPEAT_BTN
		wm.enabled |= cwiid.FLAG_NONBLOCK | cwiid.FLAG_MOTIONPLUS
		wm.rpt_mode = cwiid.RPT_STATUS | cwiid.RPT_BTN | cwiid.RPT_CLASSIC
		wm.rpt_mode |= cwiid.RPT_ACC | cwiid.RPT_EXT
		# wm.rpt_mode |= cwiid.RPT_IR
	
		return wm
	
	def register_callbacks(self, wm: Wiimote):
		def stop(*args):
			self.stop = True
			
		def activate_calibration(*args):
			wm.calibration_active = True
		
		# TODO: remove debug logs
		def debug_print(*args):
			force = 0
			for i in range(3):
				force += math.pow(wm.acc_lin[i], 2)
			force = math.sqrt(force)
			wm.logger.info("{: > 10.5f} {: > 10.5f} {: > 10.5f}, force: {: > 10.5f}".format(*wm.position, force))
		#wm.on_new_message = debug_print
		
		wm.on_disconnect = stop
		wm.on_btn_a = stop
		wm.on_btn_home = activate_calibration
	
	def run(self):
		self.wm = self.connect_wiimote()
		
		self.register_callbacks(self.wm)
		
		# vibrate when init is done
		self.wm.timed_rumble()
		
		self.logger.info('entering main loop')
		try:
			while not self.stop:
				cycle_start = time.time()
				
				self.wm.update()
				
				# print('cycle time: ' + str(time.time() - cycle_start))
				
				# sleep if needed to reach target fps
				cycle_end = cycle_start + 1 / settings.target_fps
				wait_time = cycle_end - time.time()
				if wait_time > 0:
					time.sleep(wait_time)
		except KeyboardInterrupt:
			pass
		
		self.logger.info('shutting down')
		self.close()
	
	def close(self):
		if self.midihandler is not None:
			self.midihandler.close()
		
		if self.wm is not None:
			self.wm.close()


if __name__ == '__main__':
	m = Main()
	m.run()
