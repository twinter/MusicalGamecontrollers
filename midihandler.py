import time

import mido


class MidiHandler:
	def __init__(self, channel: int = 1):
		self.channel = channel
		
		self.last_note = None
		
		midi_backend = mido.Backend(load=True)
		print('mido backends: {}'.format(midi_backend.get_output_names()))
		time.sleep(.5)
		self.midi_out: mido.ports.BaseOutput = midi_backend.open_output('amsynth:MIDI IN 134:0')
		self.midi_out.reset()
		
	def send_message(self, type, **kwargs):
		msg = mido.Message(type, **kwargs)
		self.midi_out.send(msg)
		
	def set_modulation(self, value):
		self.send_message('control_change', control=1, value=value, channel=self.channel)
		
	def note_on(self, note, velocity=64):
		self.send_message('note_on', note=note, velocity=velocity, channel=self.channel)
		
	def note_off(self, note):
		self.send_message('note_off', note=note, velocity=0, channel=self.channel)
		
	def change_note(self, note, velocity=64):
		if self.last_note is not None:
			self.note_off(self.last_note)
			
		self.note_on(note, velocity)
		self.last_note = note
		
	def close(self):
		self.midi_out.reset()
		self.midi_out.close()
